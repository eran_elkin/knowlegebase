export const data = [
  {
    subject: 'networking',
    topic: 'GET',
    details:
      'Params in URL, Can be cached, remains in history, not for sensitive data, limited length, JUST FOR RETRIEVED DATA!'
  },
  {
    subject: 'networking',
    topic: 'POST',
    details:
      'Params in BODY, Never cached, not in history, unlimited length. send data to a server to CREATE/UPDATE!'
  },
  {
    subject: 'networking',
    topic: 'Async / Defer',
    details:
      '<script> block the page to load (download and execute it). moving the script down may still be not be perfect. ' +
      'DEFER will download the script in the background and will execute it as soon it will be ready. ' +
      'ASYNC script is completely independent.'
  },
  {
    subject: 'system_design',
    topic: 'controlled uncontrolled',
    details: 'Controlled vs. Uncontrolled'
  },
  {
    subject: 'system_design',
    topic: 'pure-component',
    details: 'Pure Components'
  },
  {
    subject: 'system_design',
    topic: 'Concurrency - Dead Lock',
    details:
      'Deadlock describes a situation where two more threads are blocked because of waiting for each other forever.'
  },
  {
    subject: 'system_design',
    topic: 'Concurrency - Live Lock',
    details:
      'Livelock describes situation where two threads are busy responding to actions of each other.'
  },
  {
    subject: 'system_design',
    topic: 'Concurrency - Starvation',
    details:
      'Starvation describes a situation where a greedy thread holds a resource for a long time so other threads are blocked forever.'
  },
  {
    subject: 'system_design',
    topic: 'Networking - TCP / UDP',
    details:
      'TCP and UDP are network protocols that are used to send data packets.'
  },
  {
    subject: 'promise_deferred',
    topic: 'Promise',
    details:
      "(forward) If you are calling a function which itself returns a promise, then you don't have to create your own deferred object. You can just return that promise. In this case, the function does not create value, but forwards it."
  },
  {
    subject: 'promise_deferred',
    topic: 'Deferred',
    details: `(produce) You use deferred objects when you want to provide promise-support for your own functions. You compute a value and want to control when the promise is resolved.<br><a href="https://stackoverflow.com/questions/17308172/deferred-versus-promise">deferred-versus-promise</a>`
  },
  {
    subject: 'react',
    topic: 'React Life Cycle',
    details: `<b>Mounting</b><br>
    These methods are called in the following order when an instance of a component is being created and inserted into the DOM:
<pre><code>1) constructor()
2) static getDerivedStateFromProps()
3) render()
4) componentDidMount()</code></pre>
<b>Update</b><br>
An update can be caused by changes to props or state. These methods are called in the following order when a component is being re-rendered:
<pre><code>1) static getDerivedStateFromProps()
2) shouldComponentUpdate()
3) render()
4) getSnapshotBeforeUpdate()
5) componentDidUpdate()</code></pre>`
  },
  {
    subject: 'middleware',
    topic: 'Thunk',
    details:
      "A thunk is a function that wraps an expression to delay its evaluation.<br>Middleware extend the store's abilities, and let you write async logic that interacts with the store."
  },
  {
    subject: 'middleware',
    topic: 'Saga',
    details:
      'unlike Redux-Thunk, which utilizes callback functions, a Redux-Saga thread can be started, paused and cancelled from the main application with normal Redux actions.'
  },
  {
    subject: 'middleware',
    topic: 'Saga vs Thunk',
    details:
      'The benefit to Redux-Saga in comparison to Redux-Thunk is that you can avoid callback hell meaning that you can avoid passing in functions and calling them inside. Additionally, you can more easily test your asynchronous data flow. The call and put methods return JavaScript objects. Thus, you can simply test each value yielded by your saga function with an equality comparison. On the other hand, Redux-Thunk returns promises, which are more difficult to test. Testing thunks often requires complex mocking of the fetch api, axios requests, or other functions. With Redux-Saga, you do not need to mock functions wrapped with effects. This makes tests clean, readable and easier to write.'
  },
  {
    subject: 'es6',
    topic: 'Two Arrow Functions',
    details: `curried form<br><pre><code>const add = x => y => x + y</code></pre>
      Here is the same code without arrow functions<br>
      <pre><code>const add = function (x) {
  return function (y) {
    return x + y
  }
}</code></pre>`
  },
  {
    subject: 'firebase',
    topic: 'Firebase - Cloud Functions',
    details:
      'Functions that will run on the server. like private collection that we do not want the client to see or modify.'
  },
  {
    subject: 'react',
    topic: 'Router - Switch',
    details: `Switch - as soon as the url is feet to a pattern it will stop look for another pattern. need to add the exact (fro home page for example).
    <br><npm>npm install react-router-dom</npm>`
  },
  {
    subject: 'react',
    topic: 'HOC',
    details: `if there are more than one component with the same logic, it is good to create a reusable pattern like HOC or Render Props.<br>
      <b>HOC</b> - A pattern where a function takes a component as an argument and returns a new component.
      <pre><code>import React from 'react';

export const HOC = OriginalComponent => {
  class NewComponent extends React.Component {
    render() {
      return &lt;OriginalComponent /&gt;;
    }
  }
      
  return NewComponent;
};</code></pre>
      `
  },
  {
    subject: 'react',
    topic: 'Render Props',
    details: `if there are more than one component with the same logic, it is good to create a reusable pattern like HOC or Render Props.<br>
      <b>Render Props</b> - technique for sharing code between react components using a prop whose value is function.<br>    
      <pre><code>&lt;Counter
  render={(count, incrementCount) => (
    &lt;ClickCounter count={count} incrementCount={incrementCount} /&gt;
  )}
/&gt;</code></pre>
      `
  },
  {
    subject: 'react',
    topic: 'Functional Programing',
    details: `Functional programing is:<ul><li>Clean code</li><li>Syntactic efficient - small and efficient</li><li>More abstract - more symbolic way</li><li>Reduction of errors - prevent error to happen by cleaner code</li></ul>
      need to use pure functions:<ul><li>the same result when same params.</li><li>No side effects.</li><li>No change the state.</li></ul>
    <pre><code>let L = [1,2,3];

let add = item => item + 1;
let sum = (A, I) => { return A + I };
let val = L.map(add).reduce(sum, 0);
</code></pre>
      `
  }
];
