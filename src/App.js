import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Nav from './components/nav';
import Knowledge from './components/knowledge/knowledge';
import Projects from './components/projects/projects';
import Samples from './components/samples/samples';

import './App.css';

const App = () => {
  return (
    <Router>
      <Nav />
      <Switch>
        <Route path='/' exact component={Knowledge} />
        <Route path='/samples' exact component={Samples} />
        <Route path='/projects' component={Projects} />
      </Switch>
    </Router>
  );
};

export default App;
