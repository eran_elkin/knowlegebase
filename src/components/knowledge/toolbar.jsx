import React, { useEffect, useState, useRef } from 'react';
import firebase from '../../config/fbConfig';

function useGetOptions() {
  const [optionsList, setOptionsList] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection('knowledgeBase')
      .orderBy('createdAt', 'asc')
      .onSnapshot(snapshot => {
        const newOptionsList = snapshot.docs.map(doc => {
          return doc.data().subject;
        });
        setOptionsList([...new Set(newOptionsList)]);
      });
  }, []);

  return optionsList;
}
const Toolbar = props => {
  const [selectedTopic, setSelectedTopic] = useState('');
  const optionsList = useGetOptions() || [];
  const inputSearch = useRef();

  const onSearchHandler = ev => {
    setSelectedTopic('');
    props.onSearchHandler(ev.target.value);
  };

  const handleTopicsChange = ev => {
    inputSearch.current.value = '';
    setSelectedTopic(ev.target.value);
    props.handleTopicsChange(ev.target.value);
  };

  return (
    <div className='toolbar'>
      <label htmlFor='Search'>
        Search
        <input type='text' ref={inputSearch} onChange={onSearchHandler} />
        <label htmlFor='topics'>
          Topics
          <select value={selectedTopic} onChange={handleTopicsChange}>
            <option value=''>All Topics</option>
            {optionsList.map((item, index) => (
              <option key={index} value={item}>
                {item}
              </option>
            ))}
          </select>
        </label>
      </label>
      <div className='toolbar-btn'>
        <button className='btn-add' onClick={props.onAddRowClick}>
          Add Topic
        </button>
        <button className='btn-delete' onClick={props.onDeleteRowClick}>
          Delete Topic
        </button>
        <button className='btn-bu' onClick={props.onBackUpClick}>
          Back-up
        </button>
        <button className='btn-delete' onClick={props.onDeleteBackupClick}>
          Delete Backup
        </button>
      </div>
    </div>
  );
};

export default Toolbar;
