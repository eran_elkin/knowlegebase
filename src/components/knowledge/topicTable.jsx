import React from 'react';

const TopicTable = props => {
  return (
    <div className='container'>
      <table>
        <thead>
          <tr className='table-header'>
            <th>Topic</th>
            <th>Details</th>
          </tr>
        </thead>
        <tbody>
          {props.rows.map(row => (
            <tr key={row.topic}>
              <td
                className='td-topic'
                dangerouslySetInnerHTML={{
                  __html: `<b><span style='font-size: larger'>${row.topic}</span></b><br><span style='font-size: xx-small'>${row.id}</span>`
                }}></td>
              <td dangerouslySetInnerHTML={{ __html: row.details }}></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TopicTable;
