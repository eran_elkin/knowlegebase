import React, { useEffect, useState } from 'react';

import Toolbar from './toolbar';
import TopicTable from './topicTable';
import AddTopicForm from './addTopicForm';
import firebase from '../../config/fbConfig';
import '../../App.css';

function useKnowledgeList() {
  const [knowledge, setKnowledge] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection('knowledgeBase')
      .orderBy('createdAt', 'asc')
      .onSnapshot(snapshot => {
        const newList = snapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        }));
        setKnowledge(newList);
      });
  }, []);

  return knowledge;
}
const Knowledge = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const knowledge = useKnowledgeList() || [];
  const [filteredRows, setFilteredRows] = useState(knowledge);

  useEffect(() => {
    setFilteredRows(knowledge);
  }, [knowledge]);

  const onSearchHandler = search => {
    const searchRowsList = searchFilterData(search);
    searchHighlightedText(search, searchRowsList);
  };

  const searchFilterData = search => {
    const searchRowsList = knowledge.filter(item => {
      return item.details.toLowerCase().indexOf(search.toLowerCase()) > -1;
    });
    return searchRowsList;
  };

  const searchHighlightedText = (search, searchRowsList) => {
    const regExp = new RegExp(`${search.toLowerCase()}`);
    const rowsWithHighlighted = searchRowsList.map((item, index) => {
      return {
        ...item,
        details: item.details.replace(
          regExp,
          `<span key=${index} class='highlight'>${search}</span>`
        )
      };
    });
    setFilteredRows(rowsWithHighlighted);
  };

  const TopicsFilterData = topic => {
    const topicRowsList = knowledge.filter(item => {
      return item.subject.toLowerCase().indexOf(topic.toLowerCase()) > -1;
    });
    setFilteredRows(topicRowsList);
  };

  const handleTopicsChange = topic => {
    TopicsFilterData(topic);
  };

  const onAddRowClick = () => {
    setModalOpen(true);
  };

  const onDeleteRowClick = () => {
    const rowId = window.prompt('Type rowId to DELETE ...');
    if (rowId !== null) {
      if (window.confirm('Are you Sure?')) {
        firebase
          .firestore()
          .collection('knowledgeBase')
          .doc(rowId)
          .delete()
          .then(function() {
            console.log(`rowId: ${rowId} was DELETED`);
          });
      }
    }
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const onBackUpClick = () => {
    knowledge.forEach(row => {
      firebase
        .firestore()
        .collection('knowledgeBase-Backup')
        .add({
          subject: row.subject,
          topic: row.topic,
          details: row.details,
          createdAt: new Date().getTime()
        });
    });
    alert('Backup Process Completed');
  };

  const onDeleteBackupClick = () => {
    if (window.confirm('Delete All Backup???')) {
      firebase
        .firestore()
        .collection('knowledgeBase-Backup')
        .get()
        .then(res => {
          res.forEach(element => {
            element.ref.delete();
          });
        })
        .then(res => {
          console.log(res);
        });
    }
  };

  return (
    <div className='App'>
      <Toolbar
        onSearchHandler={onSearchHandler}
        handleTopicsChange={handleTopicsChange}
        onAddRowClick={onAddRowClick}
        onDeleteRowClick={onDeleteRowClick}
        onBackUpClick={onBackUpClick}
        onDeleteBackupClick={onDeleteBackupClick}
      />
      <TopicTable rows={filteredRows} />
      {modalOpen && <AddTopicForm onClose={closeModal} />}
    </div>
  );
};

export default Knowledge;
