import React, { useState } from 'react';
import firebase from '../../config/fbConfig';

const AddTopicForm = ({ onClose }) => {
  const [subject, setSubject] = useState('');
  const [topic, setTopic] = useState('');
  const [details, setDetails] = useState('');

  const handleSubmit = ev => {
    ev.preventDefault();

    firebase
      .firestore()
      .collection('knowledgeBase')
      .add({
        subject,
        topic,
        details,
        createdAt: new Date().getTime()
      })
      .then(() => {
        onClose();
      });
  };

  const onCancelSubmit = () => {
    onClose();
  };

  const enabledButton = () => {
    return subject !== '' && topic !== '' && details !== '';
  };
  return (
    <div className='addTopic-modal'>
      <h1 className='addTopic-title'>Add Topic</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Subject:</label>
          <input
            defaultValue={subject}
            type='text'
            onChange={ev => setSubject(ev.target.value)}
          />
        </div>
        <div>
          <label>Topic:</label>
          <input
            defaultValue={topic}
            type='text'
            onChange={ev => setTopic(ev.target.value)}
          />
        </div>
        <div>
          <label>Topic Details:</label>
          <textarea
            rows='6'
            cols='70'
            defaultValue={details}
            type='textarea'
            onChange={ev => setDetails(ev.target.value)}
          />
        </div>
        <div className='modal-buttons'>
          <button onClick={handleSubmit} disabled={!enabledButton()}>
            Add item
          </button>
          <button onClick={onCancelSubmit}>Cancel</button>
        </div>
      </form>
    </div>
  );
};

export default AddTopicForm;
