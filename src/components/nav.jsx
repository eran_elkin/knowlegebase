import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

function Nav() {
  return (
    <nav>
      <img className='toolbar-img' src='/kb.png' alt='stam' />
      <ul className='nav-links'>
        <Link className='nav-link' to='/'>
          <li>Knowledge Base</li>
        </Link>
        <Link className='nav-link' to='/samples'>
          <li>Samples</li>
        </Link>
        <Link className='nav-link' to='/projects'>
          <li>Projects</li>
        </Link>
      </ul>
    </nav>
  );
}

export default Nav;
