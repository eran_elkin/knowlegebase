import React from 'react';

const ProjectsTable = props => {
  return (
    <div className='project-container'>
      <table>
        <thead>
          <tr className='table-header'>
            <th>Project</th>
            <th>Description</th>
            <th>Link</th>
          </tr>
        </thead>
        <tbody>
          {props.projects.map(project => (
            <tr key={project.name}>
              <td
                className='td-name'
                dangerouslySetInnerHTML={{
                  __html: `<b><span style='font-size: larger'>${project.name}</span></b><br><span style='font-size: xx-small'>${project.id}</span>`
                }}></td>
              <td
                dangerouslySetInnerHTML={{ __html: project.description }}></td>
              <td>
                <a href='https://www.google.com'>hi</a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ProjectsTable;
