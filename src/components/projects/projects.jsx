import React, { useEffect, useState } from 'react';
import firebase from '../../config/fbConfig';
import ProjectsTable from './projectsTable';

function useProjectsList() {
  const [projects, setProjects] = useState([]);

  useEffect(() => {
    firebase
      .firestore()
      .collection('projectsList')
      .onSnapshot(snapshot => {
        const newList = snapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data()
        }));
        setProjects(newList);
      });
  }, []);

  return projects;
}

function Projects() {
  const projects = useProjectsList();

  return (
    <div className='projects'>
      <ProjectsTable projects={projects} />
    </div>
  );
}

export default Projects;
