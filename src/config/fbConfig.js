import firebase from 'firebase/app';
import 'firebase/firestore';

var firebaseConfig = {
  apiKey: 'AIzaSyAVirIP2OwstdBXpimhryHnWCG2cDLGqe0',
  authDomain: 'knowlagebase-ee12b.firebaseapp.com',
  databaseURL: 'https://knowlagebase-ee12b.firebaseio.com',
  projectId: 'knowlagebase-ee12b',
  storageBucket: 'knowlagebase-ee12b.appspot.com',
  messagingSenderId: '287781505271',
  appId: '1:287781505271:web:cfe3ff1c01e706f9172bf2'
};
firebase.initializeApp(firebaseConfig);

export default firebase;
